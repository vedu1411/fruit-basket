module.exports.speech=[
`<audio src="https://s3.amazonaws.com/fruitspeech/s-mango.mp3"/>`
,`<audio src="https://s3.amazonaws.com/fruitspeech/apple-sam.mp3"/> <break time="1s"/> <audio src="https://s3.amazonaws.com/fruitspeech/s-apple.mp3"/>`
,`<audio src="https://s3.amazonaws.com/fruitspeech/straw-sam.mp3"/> <break time="1s"/> <audio src="https://s3.amazonaws.com/fruitspeech/s-straw.mp3"/>`
,`<audio src="https://s3.amazonaws.com/fruitspeech/oran-sam.mp3"/> <break time="1s"/> <audio src="https://s3.amazonaws.com/fruitspeech/s-oran.mp3"/>`
,`<audio src="https://s3.amazonaws.com/fruitspeech/water-sam.mp3"/> <break time="1s"/><audio src="https://s3.amazonaws.com/fruitspeech/s-water.mp3"/>`
,`<audio src="https://s3.amazonaws.com/fruitspeech/banan-sam.mp3"/>  <break time="1s"/> Hello there! I'm the Banana! I’m a fruit that’s curved, long and thin. I'm covered with soft yellow skin. I grow all around the year. Am I not awesome! My seeds are barely visible. Have you ever seen banana seeds?`
,`<audio src="https://s3.amazonaws.com/fruitspeech/pine-sam.mp3"/> <break time="1s"/> <audio src="https://s3.amazonaws.com/fruitspeech/s-pine.mp3"/>`
,`<audio src="https://s3.amazonaws.com/fruitspeech/grap-sam.mp3"/> <break time="1s"/> <audio src="https://s3.amazonaws.com/fruitspeech/s-grap.mp3"/>`
,`<audio src="https://s3.amazonaws.com/fruitspeech/guav-sam.mp3"/> <break time="1s"/> <audio src="https://s3.amazonaws.com/fruitspeech/s-guav.mp3"/>`
,`<audio src="https://s3.amazonaws.com/fruitspeech/peac-sam.mp3"/> <break time="1s"/> <audio src="https://s3.amazonaws.com/fruitspeech/s-peac.mp3"/>`
]
