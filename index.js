"use strict";

const Alexa = require('alexa-sdk');
const makePlainText = Alexa.utils.TextUtils.makePlainText;
const makeImage = Alexa.utils.ImageUtils.makeImage;

let speechOutput;
let reprompt;
let response;
let ans;
let welcomeOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/speech-1-1.mp3"/> <audio src="https://s3.amazonaws.com/fallingfruit/lxYY3adJ-wooden-thump-soundbiblecom-1322361482.mp3"/> <audio src="https://s3.amazonaws.com/fruitspeech/speech-1-2.mp3"/>`;
let welcomeReprompt = `<audio src="https://s3.amazonaws.com/fruitspeech/rewel.mp3"/>`;


const APP_ID = undefined;
const quiz=require('./quiz');
const speech=require('./speech');
speechOutput = '';
var i,k,x;
const n=0;
let imgurl,audurl;
const handlers = {

	'LaunchRequest': function () {
this.attributes.i = 1;
this.attributes.k=0;
audurl = `<audio src="https://s3.amazonaws.com/colorskill/audio/cheerful.mp3" />`;
imgurl = 'https://s3.amazonaws.com/fruitsskill/ff.jpg';
const builder = new Alexa.templateBuilders.BodyTemplate6Builder();

const template = builder.setTitle('')
								.setBackgroundImage(makeImage(imgurl))
								.setTextContent(makePlainText(''))
								.build();

if (this.event.context.System.device.supportedInterfaces.Display) {
		this.response.hint('Next', 'PlainText');
		this.response.renderTemplate(template).speak(audurl +welcomeOutput).listen(welcomeReprompt).shouldEndSession(false);
		this.emit(':responseReady');
	}
	else{
		this.emit(':ask', welcomeOutput, welcomeReprompt);
		// this.response.speak('This skill was specifically designed for the echo show. And echo spot.').shouldEndSession(true);
		// this.emit(':responseReady');
	}

	},

	'AMAZON.HelpIntent': function () {
		speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/help.mp3"/>`;
		reprompt = `<audio src="https://s3.amazonaws.com/fruitspeech/help.mp3"/>`;
		imgurl = 'https://s3.amazonaws.com/fruitbasket/sam.png';
		const builder = new Alexa.templateBuilders.BodyTemplate6Builder();

		const template = builder.setTitle('')
										.setBackgroundImage(makeImage(imgurl))
										.setTextContent(makePlainText(''))
										.build();

		if (this.event.context.System.device.supportedInterfaces.Display) {
				this.response.hint('', 'PlainText');
				this.response.renderTemplate(template).speak( speechOutput).listen(reprompt).shouldEndSession(false);
				this.emit(':responseReady');
			}
			else{
				this.emit(':ask', speechOutput, reprompt);
				// this.response.speak('This skill was specifically designed for the echo show. And echo spot.').shouldEndSession(true);
				// this.emit(':responseReady');
			}
	},

   'AMAZON.CancelIntent': function () {
		speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/cancel.mp3"/>`;
		reprompt=`<audio src="https://s3.amazonaws.com/fruitspeech/cancel.mp3"/>`;
		imgurl = 'https://s3.amazonaws.com/fruitbasket/sam.png';
		const builder = new Alexa.templateBuilders.BodyTemplate6Builder();

		const template = builder.setTitle('')
										.setBackgroundImage(makeImage(imgurl))
										.setTextContent(makePlainText(''))
										.build();

		if (this.event.context.System.device.supportedInterfaces.Display) {
				this.response.hint('', 'PlainText');
				this.response.renderTemplate(template).speak( speechOutput).listen(reprompt).shouldEndSession(true);
				this.emit(':responseReady');
			}
			else{
				this.emit(':tell', speechOutput, reprompt);
				// this.response.speak('This skill was specifically designed for the echo show. And echo spot.').shouldEndSession(true);
				// this.emit(':responseReady');
			}
	},

   'AMAZON.StopIntent': function () {
		 speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/cancel.mp3"/>`;
		 reprompt=`<audio src="https://s3.amazonaws.com/fruitspeech/cancel.mp3"/>`;
		 imgurl = 'https://s3.amazonaws.com/fruitbasket/sam.png';
		 const builder = new Alexa.templateBuilders.BodyTemplate6Builder();

		 const template = builder.setTitle('')
		 								.setBackgroundImage(makeImage(imgurl))
		 								.setTextContent(makePlainText(''))
		 								.build();

		 if (this.event.context.System.device.supportedInterfaces.Display) {
		 		this.response.hint('', 'PlainText');
		 		this.response.renderTemplate(template).speak( speechOutput).listen(reprompt).shouldEndSession(true);
		 		this.emit(':responseReady');
		 	}
		 	else{
		 		this.emit(':tell', speechOutput, reprompt);
		 		// this.response.speak('This skill was specifically designed for the echo show. And echo spot.').shouldEndSession(true);
		 		// this.emit(':responseReady');
		 	}
   },

   'SessionEndedRequest': function () {
		speechOutput = '';
		//this.emit(':saveState', true);//uncomment to save attributes to db on session end
		this.emit(':tell', speechOutput);
   },

	'AMAZON.FallbackIntent': function () {
		speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/fallback.mp3"/>`;
		reprompt=`<audio src="https://s3.amazonaws.com/fruitspeech/fallback.mp3"/>`;
		imgurl = 'https://s3.amazonaws.com/fruitbasket/sam.png';
		const builder = new Alexa.templateBuilders.BodyTemplate6Builder();

		const template = builder.setTitle('')
										.setBackgroundImage(makeImage(imgurl))
										.setTextContent(makePlainText(''))
										.build();

		if (this.event.context.System.device.supportedInterfaces.Display) {
				this.response.hint('Help', 'PlainText');
				this.response.renderTemplate(template).speak( speechOutput).listen(reprompt).shouldEndSession(false);
				this.emit(':responseReady');
			}
			else{
				this.emit(':ask', speechOutput, reprompt);
				// this.response.speak('This skill was specifically designed for the echo show. And echo spot.').shouldEndSession(true);
				// this.emit(':responseReady');
			}
    },

	'AMAZON.YesIntent': function () {
		const builder = new Alexa.templateBuilders.BodyTemplate6Builder();
		var speechOutput = '';
	if(this.attributes.i===1){ speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/y-mango.mp3"/>`;
	response=`<audio src="https://s3.amazonaws.com/fruitspeech/re-mango.mp3"/>`;
	ans='Next';
	imgurl='https://s3.amazonaws.com/fruitsskill/mango-02-min.jpg';
		this.attributes.i=this.attributes.i+1;
		this.attributes.k=this.attributes.k+1;
	}else if(this.attributes.i===2){
		speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/y-apple.mp3"/>`;
 response=`<audio src="https://s3.amazonaws.com/fruitspeech/re-apple.mp3"/>`;
 ans='Next';
 imgurl='https://s3.amazonaws.com/fruitsskill/apple-02-min.jpg';
		this.attributes.i=this.attributes.i+1;
		this.attributes.k=this.attributes.k+1;
	}else if(this.attributes.i===3){
		speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/y-straw.mp3"/>`;
 response=`<audio src="https://s3.amazonaws.com/fruitspeech/re-straw.mp3"/>`;
 ans='Next';
 imgurl='https://s3.amazonaws.com/fruitsskill/strawberry-02-min.jpg';
		this.attributes.i=this.attributes.i+1;
		this.attributes.k=this.attributes.k+1;
	}else if(this.attributes.i===4){
		speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/y-oran.mp3"/>`;
 response=`<audio src="https://s3.amazonaws.com/fruitspeech/re-oran.mp3"/>`;
 ans='Next';
 imgurl='https://s3.amazonaws.com/fruitsskill/orange-02-min.jpg';
		this.attributes.i=this.attributes.i+1;
		this.attributes.k=this.attributes.k+1;
	}else if(this.attributes.i===5){
		speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/yn-water.mp3"/>`;
 response=`<audio src="https://s3.amazonaws.com/fruitspeech/re-water.mp3"/>`;
 ans='Next';
 imgurl='https://s3.amazonaws.com/fruitsskill/watermelon-02-min.jpg';
		this.attributes.i=this.attributes.i+1;
		this.attributes.k=this.attributes.k+1;
	}else if(this.attributes.i===6){
		speechOutput = `They are the small black dots on me. Very small, but visible. <break time="1s"/> Please Say next to move to next fruit.`;
 response='Please say next to move to next fruit.';
 ans='Next';
 imgurl='https://s3.amazonaws.com/fruitsskill/banana-02-min.jpg';
		this.attributes.i=this.attributes.i+1;
		this.attributes.k=this.attributes.k+1;
	}else if(this.attributes.i===7){
		speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/y-pine.mp3"/>`;
 response=`<audio src="https://s3.amazonaws.com/fruitspeech/re-pine.mp3"/>`;
 ans='Next';
 imgurl='https://s3.amazonaws.com/fruitsskill/pineapple-min.jpg';
		this.attributes.i=this.attributes.i+1;
		this.attributes.k=this.attributes.k+1;
	}else if(this.attributes.i===8){
		speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/yn-grap.mp3"/>`;
 response=`<audio src="https://s3.amazonaws.com/fruitspeech/re-grap.mp3"/>`;
 ans='Next';
 imgurl='https://s3.amazonaws.com/fruitsskill/grapes-02-min.jpg';
		this.attributes.i=this.attributes.i+1;
		this.attributes.k=this.attributes.k+1;
	}else if(this.attributes.i===9){
		speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/y-guav.mp3"/>`;
 response=`<audio src="https://s3.amazonaws.com/fruitspeech/re-guav.mp3"/>`;
 ans='Next';
 imgurl='https://s3.amazonaws.com/fruitsskill/guava-02-min.jpg';
		this.attributes.i=this.attributes.i+1;
		this.attributes.k=this.attributes.k+1;
	}else if(this.attributes.i===10){
		speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/y-peac.mp3"/> <break time="2s"/> <audio src="https://s3.amazonaws.com/fruitspeech/quiz.mp3"/>`;
 response=`<audio src="https://s3.amazonaws.com/fruitspeech/re-peac.mp3"/>`;
 ans='Quiz';
 imgurl='https://s3.amazonaws.com/fruitsskill/peach-02-min.jpg';
		this.attributes.i=this.attributes.i+1;
		this.attributes.k=this.attributes.k+1;
	}
	else {
			speechOutput='';
			response='';
			imgurl='https://s3.amazonaws.com/fruitbasket/sam.png';
	}
	const template = builder.setTitle('')
  								.setBackgroundImage(makeImage(imgurl))
  								.setTextContent(makePlainText(''))
  								.build();
	if (this.event.context.System.device.supportedInterfaces.Display) {
			this.response.hint('', 'PlainText');
			this.response.renderTemplate(template).speak(speechOutput).listen(response).shouldEndSession(false);
			this.emit(':responseReady');
		}
		else{
			this.emit(":ask", speechOutput, response);
										// this.response.speak('This skill was specifically designed for the echo show. And echo spot.').shouldEndSession(true);
			// this.emit(':responseReady');
		}
    },

	'AMAZON.NoIntent': function () {
		const builder = new Alexa.templateBuilders.BodyTemplate6Builder();
		var speechOutput = '';
		if(this.attributes.i===1){
			speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/n-mango.mp3"/>`;
			response=`<audio src="https://s3.amazonaws.com/fruitspeech/re-mango.mp3"/>`;
			ans='Next';
			imgurl='https://s3.amazonaws.com/fruitsskill/mango-02-min.jpg';
				 this.attributes.i=this.attributes.i+1;
				 this.attributes.k=this.attributes.k+1;
		}else if(this.attributes.i===2){
			speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/n-apple.mp3"/>`;
			response=`<audio src="https://s3.amazonaws.com/fruitspeech/re-apple.mp3"/>`;
			ans='Next';
			imgurl='https://s3.amazonaws.com/fruitsskill/apple-02-min.jpg';
				 this.attributes.i=this.attributes.i+1;
				 this.attributes.k=this.attributes.k+1;
		}else if(this.attributes.i===3){
			speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/n-straw.mp3"/>`;
			response=`<audio src="https://s3.amazonaws.com/fruitspeech/re-straw.mp3"/>`;
			ans='Next';
			imgurl='https://s3.amazonaws.com/fruitsskill/strawberry-02-min.jpg';
				 this.attributes.i=this.attributes.i+1;
				 this.attributes.k=this.attributes.k+1;
		}else if(this.attributes.i===4){
			speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/n-oran.mp3"/>`;
			response=`<audio src="https://s3.amazonaws.com/fruitspeech/re-oran.mp3"/>`;
			ans='Next';
			imgurl='https://s3.amazonaws.com/fruitsskill/orange-02-min.jpg';
				 this.attributes.i=this.attributes.i+1;
				 this.attributes.k=this.attributes.k+1;
		}else if(this.attributes.i===5){
			speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/yn-water.mp3"/>`;
			response=`<audio src="https://s3.amazonaws.com/fruitspeech/re-water.mp3"/>`;
			ans='Next';
			imgurl='https://s3.amazonaws.com/fruitsskill/watermelon-02-min.jpg';
				 this.attributes.i=this.attributes.i+1;
				 this.attributes.k=this.attributes.k+1;
		}else if(this.attributes.i===6){
			speechOutput = `They are the small black dots on me. Very small, but visible. <break time="1s"/> Please Say next to move to next fruit.`;
			response='Please say next to move to next fruit.';
			ans='Next';
			imgurl='https://s3.amazonaws.com/fruitsskill/banana-02-min.jpg';
				 this.attributes.i=this.attributes.i+1;
				 this.attributes.k=this.attributes.k+1;
		}else if(this.attributes.i===7){
			speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/n-pine.mp3"/>`;
			response=`<audio src="https://s3.amazonaws.com/fruitspeech/re-pine.mp3"/>`;
			ans='Next';
			imgurl='https://s3.amazonaws.com/fruitsskill/pineapple-min.jpg';
				 this.attributes.i=this.attributes.i+1;
				 this.attributes.k=this.attributes.k+1;
		}else if(this.attributes.i===8){
			speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/yn-grap.mp3"/>`;
			response=`<audio src="https://s3.amazonaws.com/fruitspeech/re-grap.mp3"/>`;
			ans='Next';
			imgurl='https://s3.amazonaws.com/fruitsskill/grapes-02-min.jpg';
				 this.attributes.i=this.attributes.i+1;
				 this.attributes.k=this.attributes.k+1;
		}else if(this.attributes.i===9){
			speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/n-guav.mp3"/>`;
			response=`<audio src="https://s3.amazonaws.com/fruitspeech/re-guav.mp3"/>`;
			ans='Next';
			imgurl='https://s3.amazonaws.com/fruitsskill/guava-02-min.jpg';
				 this.attributes.i=this.attributes.i+1;
				 this.attributes.k=this.attributes.k+1;
		}else if(this.attributes.i===10){
			speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/n-peac.mp3"/><break time="2s"/> <audio src="https://s3.amazonaws.com/fruitspeech/quiz.mp3"/>`;
			response=`<audio src="https://s3.amazonaws.com/fruitspeech/re-peac.mp3"/>`;
			ans='Quiz';
			imgurl='https://s3.amazonaws.com/fruitsskill/peach-02-min.jpg';
				 this.attributes.i=this.attributes.i+1;
				 this.attributes.k=this.attributes.k+1;
		}
		else {
				speechOutput='';
				response='';
				imgurl='https://s3.amazonaws.com/fruitbasket/sam.png';
		}
		const template = builder.setTitle('')
	  								.setBackgroundImage(makeImage(imgurl))
	  								.setTextContent(makePlainText(''))
	  								.build();
		if (this.event.context.System.device.supportedInterfaces.Display) {
				this.response.hint('', 'PlainText');
				this.response.renderTemplate(template).speak(speechOutput).listen(response).shouldEndSession(false);
				this.emit(':responseReady');
			}
			else{
				this.emit(":ask", speechOutput, response);
											// this.response.speak('This skill was specifically designed for the echo show. And echo spot.').shouldEndSession(true);
				// this.emit(':responseReady');
			}
    },

	'fruitIntent': function () {
		const builder = new Alexa.templateBuilders.BodyTemplate6Builder();
		speechOutput = '';
		if(this.attributes.k===0){
			imgurl='https://s3.amazonaws.com/fruitsskill/mango-02-min.jpg';
		}
		else if(this.attributes.k===1){
			imgurl='https://s3.amazonaws.com/fruitsskill/apple-02-min.jpg';
		}
		else if(this.attributes.k===2){
			imgurl='https://s3.amazonaws.com/fruitsskill/strawberry-02-min.jpg';
		}
		else if(this.attributes.k===3){
			imgurl='https://s3.amazonaws.com/fruitsskill/orange-02-min.jpg';
		}
		else if(this.attributes.k===4){
			imgurl='https://s3.amazonaws.com/fruitsskill/watermelon-02-min.jpg';
		}
		else if(this.attributes.k===5){
			imgurl='https://s3.amazonaws.com/fruitsskill/banana-02-min.jpg';
		}
		else if(this.attributes.k===6){
			imgurl='https://s3.amazonaws.com/fruitsskill/pineapple-min.jpg';
		}
		else if(this.attributes.k===7){
			imgurl='https://s3.amazonaws.com/fruitsskill/grapes-02-min.jpg';
		}
		else if(this.attributes.k===8){
			imgurl='https://s3.amazonaws.com/fruitsskill/guava-02-min.jpg';
		}
		else if(this.attributes.k===9){
			imgurl='https://s3.amazonaws.com/fruitsskill/peach-02-min.jpg';
		}
		else{
			imgurl='https://s3.amazonaws.com/fruitbasket/sam.png';
		}
		const template = builder.setTitle('')
	  								.setBackgroundImage(makeImage(imgurl))
	  								.setTextContent(makePlainText(''))
	  								.build();

	  if (this.event.context.System.device.supportedInterfaces.Display) {
	  		this.response.hint('', 'PlainText');
	  		this.response.renderTemplate(template).speak(speech.speech[this.attributes.k]).listen(speech.speech[this.attributes.k]).shouldEndSession(false);
	  		this.emit(':responseReady');
	  	}
	  	else{
	  		this.emit(':ask', speech.speech[this.attributes.k], speech.speech[this.attributes.k]);
	  		// this.response.speak('This skill was specifically designed for the echo show. And echo spot.').shouldEndSession(true);
	  		// this.emit(':responseReady');
	  	}
},

'quizIntent': function () {
	imgurl = 'https://s3.amazonaws.com/fruitsskill/kitchen-03-min.jpg';
	const builder = new Alexa.templateBuilders.BodyTemplate6Builder();

	const template = builder.setTitle('')
									.setBackgroundImage(makeImage(imgurl))
									.setTextContent(makePlainText(''))
									.build();

if(!('x' in this.attributes))
		{
		 this.attributes.x=0;

	 }
	 if(this.attributes.x===0){
		 if (this.event.context.System.device.supportedInterfaces.Display) {
	       this.response.hint('', 'PlainText');
	       this.response.renderTemplate(template).speak( quiz.quiz[(this.attributes.x)++].Question).listen(quiz.quiz[(this.attributes.x)].Question).shouldEndSession(false);
	       this.emit(':responseReady');
	     }
	     else{
	       			this.emit(":ask",quiz.quiz[(this.attributes.x)++].Question,quiz.quiz[(this.attributes.x)].Question);
	       // this.response.speak('This skill was specifically designed for the echo show. And echo spot.').shouldEndSession(true);
	       // this.emit(':responseReady');
	     }
			}
	 else{
				let fruitSlotRaw = this.event.request.intent.slots.fruit.value;
				if(fruitSlotRaw===quiz.quiz[(this.attributes.x)-1].Answer){
					if(this.attributes.x===10){
						if (this.event.context.System.device.supportedInterfaces.Display) {
					      this.response.hint('', 'PlainText');
					      this.response.renderTemplate(template).speak( `<audio src="https://s3.amazonaws.com/colorskill/audio/claps.mp3"/> <audio src="https://s3.amazonaws.com/fruitspeech/end-1.mp3"/> <audio src="https://s3.amazonaws.com/colorskill/audio/goodvibes.mp3" />`).listen('').shouldEndSession(true);
					      this.emit(':responseReady');
					    }
					    else{
					this.emit(":tell",`<audio src="https://s3.amazonaws.com/colorskill/audio/claps.mp3"/> <audio src="https://s3.amazonaws.com/fruitspeech/end-1.mp3"/> <audio src="https://s3.amazonaws.com/colorskill/audio/goodvibes.mp3" />`);
					      // this.response.speak('This skill was specifically designed for the echo show. And echo spot.').shouldEndSession(true);
					      // this.emit(':responseReady');
					    }
					}
					if (this.event.context.System.device.supportedInterfaces.Display) {
				      this.response.hint('', 'PlainText');
				      this.response.renderTemplate(template).speak( ` <audio src="https://s3.amazonaws.com/colorskill/audio/claps.mp3"/> <audio src="https://s3.amazonaws.com/fruitspeech/end-2.mp3"/> ${quiz.quiz[(this.attributes.x)++].Question}`).listen('').shouldEndSession(false);
				      this.emit(':responseReady');
				    }
				    else{
									this.emit(":ask",` <audio src="https://s3.amazonaws.com/colorskill/audio/claps.mp3"/> <audio src="https://s3.amazonaws.com/fruitspeech/end-2.mp3"/> ${quiz.quiz[(this.attributes.x)++].Question}`);
				      // this.response.speak('This skill was specifically designed for the echo show. And echo spot.').shouldEndSession(true);
				      // this.emit(':responseReady');
				    }

					}
				else
				{
					if(this.attributes.x===10){
						if (this.event.context.System.device.supportedInterfaces.Display) {
					      this.response.hint('', 'PlainText');
					      this.response.renderTemplate(template).speak(`<audio src="https://s3.amazonaws.com/colorskill/audio/trombone+wrong+ans.mp3"/> <audio src="https://s3.amazonaws.com/fruitspeech/end-3.mp3"/> <audio src="https://s3.amazonaws.com/colorskill/audio/goodvibes.mp3" />`).listen('').shouldEndSession(true);
					      this.emit(':responseReady');
					    }
					    else{
					    this.emit(":tell",`<audio src="https://s3.amazonaws.com/colorskill/audio/trombone+wrong+ans.mp3"/> <audio src="https://s3.amazonaws.com/fruitspeech/end-3.mp3"/> <audio src="https://s3.amazonaws.com/colorskill/audio/goodvibes.mp3" />`);
					      // this.response.speak('This skill was specifically designed for the echo show. And echo spot.').shouldEndSession(true);
					      // this.emit(':responseReady');
					    }
					}
					if (this.event.context.System.device.supportedInterfaces.Display) {
				      this.response.hint('', 'PlainText');
				      this.response.renderTemplate(template).speak(`<audio src="https://s3.amazonaws.com/colorskill/audio/trombone+wrong+ans.mp3"/> <audio src="https://s3.amazonaws.com/fruitspeech/end-4.mp3"/> ${quiz.quiz[(this.attributes.x)-1].Audio} <audio src="https://s3.amazonaws.com/fruitspeech/nq.mp3"/> ${quiz.quiz[(this.attributes.x)++].Question}`).listen('').shouldEndSession(false);
				      this.emit(':responseReady');
				    }
				    else{
				    					this.emit(":ask",`<audio src="https://s3.amazonaws.com/colorskill/audio/trombone+wrong+ans.mp3"/> <audio src="https://s3.amazonaws.com/fruitspeech/end-4.mp3"/> ${quiz.quiz[(this.attributes.x)-1].Audio} <audio src="https://s3.amazonaws.com/fruitspeech/nq.mp3"/> ${quiz.quiz[(this.attributes.x)++].Question}`);
				                    // this.response.speak('This skill was specifically designed for the echo show. And echo spot.').shouldEndSession(true);
				      // this.emit(':responseReady');
				    }
							}
			}
			this.emit(":ask",)
    },
	'Unhandled': function () {
        speechOutput = `<audio src="https://s3.amazonaws.com/fruitspeech/fallback.mp3"/>`;
				reprompt= `<audio src="https://s3.amazonaws.com/fruitspeech/fallback.mp3"/>`;
				imgurl = 'https://s3.amazonaws.com/fruitbasket/sam.png';
				const builder = new Alexa.templateBuilders.BodyTemplate6Builder();

				const template = builder.setTitle('')
												.setBackgroundImage(makeImage(imgurl))
												.setTextContent(makePlainText(''))
												.build();

				if (this.event.context.System.device.supportedInterfaces.Display) {
						this.response.hint('Help', 'PlainText');
						this.response.renderTemplate(template).speak( speechOutput).listen(reprompt).shouldEndSession(false);
						this.emit(':responseReady');
					}
					else{
						this.emit(':ask', speechOutput, reprompt);
						// this.response.speak('This skill was specifically designed for the echo show. And echo spot.').shouldEndSession(true);
						// this.emit(':responseReady');
					}
    }
};

exports.handler = function(event, context, callback){
    const alexa = Alexa.handler(event, context, callback);
    alexa.appId = undefined;
    alexa.registerHandlers(handlers);
    alexa.execute();
};

function randomInt(num) {
    return Math.floor(Math.random()*num) ;
}
