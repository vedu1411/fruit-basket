module.exports.quiz=[
  {
    "Question":`<audio src="https://s3.amazonaws.com/fruitspeech/quizq-oran.mp3"/>`,
    "Answer":`orange`,
    "Audio":`<audio src="https://s3.amazonaws.com/fruitspeech/quiza-oran.mp3"/>`
  },
  {
    "Question":`<audio src="https://s3.amazonaws.com/fruitspeech/quizq-straw.mp3"/>`,
    "Answer":`strawberry`,
    "Audio":`<audio src="https://s3.amazonaws.com/fruitspeech/quiza-straw.mp3"/>`
  },
  {
    "Question":`<audio src="https://s3.amazonaws.com/fruitspeech/quizq-apple.mp3"/>`,
    "Answer":`apple`,
    "Audio":`<audio src="https://s3.amazonaws.com/fruitspeech/quiza-apple.mp3"/>`
  },
  {
    "Question":`<audio src="https://s3.amazonaws.com/fruitspeech/quizq-water.mp3"/>`,
    "Answer":`watermelon`,
    "Audio":`<audio src="https://s3.amazonaws.com/fruitspeech/quiza-water.mp3"/>`
  },
  {
    "Question":"My seeds are barely visible to eyes? Which fruit am I?",
    "Answer":`banana`,
    "Audio":"banana"
  },
  {
    "Question":`<audio src="https://s3.amazonaws.com/fruitspeech/quizq-pine.mp3"/>`,
    "Answer":`pineapple`,
    "Audio":`<audio src="https://s3.amazonaws.com/fruitspeech/quiza-pine.mp3"/>`
  },
  {
    "Question":`<audio src="https://s3.amazonaws.com/fruitspeech/quizq-grap.mp3"/>`,
    "Answer":`grapes`,
    "Audio":`<audio src="https://s3.amazonaws.com/fruitspeech/quiza-grap.mp3"/>`
  },
  {
    "Question":`<audio src="https://s3.amazonaws.com/fruitspeech/quizq-guav.mp3"/>`,
    "Answer":`guava`,
    "Audio":`<audio src="https://s3.amazonaws.com/fruitspeech/quiza-guav.mp3"/>`
  },
  {
    "Question":`<audio src="https://s3.amazonaws.com/fruitspeech/quizq-peac.mp3"/>`,
    "Answer":`peach`,
    "Audio":`<audio src="https://s3.amazonaws.com/fruitspeech/quiza-peac.mp3"/>`
  },
  {
    "Question":`<audio src="https://s3.amazonaws.com/fruitspeech/quizq-mango.mp3"/>`,
    "Answer":`mango`,
    "Audio":`<audio src="https://s3.amazonaws.com/fruitspeech/quiza-mango.mp3"/>`
  }
]
